document.addEventListener("DOMContentLoaded", function () {
  animateSectionOnLoad(document.querySelector(".fade-up"));

  window.addEventListener("scroll", function () {
    var sections = document.querySelectorAll(".fade-up");
    var screenHeight = window.innerHeight;

    sections.forEach(function (section) {
      var positionFromTop = section.getBoundingClientRect().top;

      if (positionFromTop < screenHeight * 0.75) {
        section.classList.add("active");
      }
    });
  });
});

function animateSectionOnLoad(section) {
  section.classList.add("active");
}
